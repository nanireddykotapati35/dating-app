import "./edit_profile.css";

const Editprofile = () => {
  {
    var loadFile = function (event) {
      var image = document.getElementById("output");
      image.src = URL.createObjectURL(event.target.files[0]);
    };
  }
  return (
    <div id="sc-edprofile">
      <h1>Edit Profile Form</h1>

      <div class="profile-pic">
        <label class="-label" for="file">
          {/* <span class="glyphicon glyphicon-camera"></span> */}
          <span>Change Image</span>
        </label>
        <input id="file" type="file" onchange="loadFile(event)" />
        {/* <img src="https://cdn.pixabay.com/photo/2017/08/06/21/01/louvre-2596278_960_720.jpg" id="output" width="200" /> */}
      </div>
      <div class="sc-container">
        <input type="text" placeholder="Full Name" />
        <input type="text" placeholder="Username" />
        <input type="text" placeholder="Email Address" />
        <input type="password" placeholder="Password" />
        <input type="text" placeholder="Interested in" />

        <textarea placeholder="Passion"></textarea>
        <select>
          <option value="">Male</option>
          <option value="">Female</option>
          <option value="">other</option>
        </select>

        <input type="submit" value="save changes" />
      </div>
    </div>
  );
};

export default Editprofile;
