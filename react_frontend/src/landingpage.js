import React from "react";
import "./landingpage.css";
import "../static_files/images/pngfind.com-tinder-logo-png-131053.png";

function Landingpage() {
  return (
    <div class="header">
      <nav>
        <img
          src="../images/pngfind.com-tinder-logo-png-131053.png"
          class="logo"
        />
        <ul>
          <li>
            <a href="#">learn</a>
          </li>
          <li>
            <a href="#">support</a>
          </li>
          <li>
            <a href="#">Download</a>
          </li>
          <li>
            <button id="login" onclick="window.location.href='login.html'">
              Login
            </button>
          </li>
        </ul>
      </nav>

      <div class="text-box">
        <h1>start something epic!</h1>

        <button id="createbtn" onclick="window.location.href='register.html'">
          create account
        </button>
      </div>
      <a
        href="https://play.google.com/store/apps/details?id=com.tinder&hl=en"
        class="download"
      >
        Download
      </a>
    </div>
  );
}

export default Landingpage;
